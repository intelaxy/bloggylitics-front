import React from 'react'

import s from './Button.module.css'

export const Button = ({ text, onClick }) => {
  return (
    <button
      className={s.root}
      onClick={onClick}
    >
      {text}
    </button>
  )
}
