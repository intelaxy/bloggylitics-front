# Bloggylitics

Intelaxy test task


### Installing

Clone the repo

```
git clone git@bitbucket.org:intelaxy/bloggylitics-front.git
```

Install packages

```
npm i
```

### Run project

```
npm start
```

### Open project in browser 

```
http://localhost:6006
```

### How to

1. Read how to make basic components in React: [https://reactjs.org/docs/components-and-props.html]()
2. Read how to use Storybook: [https://storybook.js.org/docs/basics/introduction/]()
3. All new components must be placed in the src/components
4. All components must be capitalized
5. If necessary use Button component as a template
6. Pixel perfect

### When task is done

Send archive with src and stories folders